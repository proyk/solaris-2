import React, { Component } from 'react';
import OrderFilter from './OrderFilter';
import GenusFilter from './GenusFilter';
import SpeciesFilter from './SpeciesFilter';
import axios from 'axios';
import Loading from './Loading';

class TaxonomyFilter extends Component {
  constructor() {
    super();

    this.state = {
      taxonomy: {
        order: [],
        genus: [],
        species: [],
      },
      loading: true,
      order: '',
      genus: '',
      species: '',
    };

    this.handleChangeOrder = this.handleChangeOrder.bind(this);
    this.handleChangeGenus = this.handleChangeGenus.bind(this);
    this.handleChangeSpecies = this.handleChangeSpecies.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.jsonRequest =
      axios
      .get('https://s3-eu-west-1.amazonaws.com/proyk-uploads/taxonomy.json')
      .then((result) => {
        this.setState({
          taxonomy: result.data || [],
          loading: false,
        });
      });
  }

  componentWillUnmount() {
    this.jsonRequest.abort();
  }

  handleChangeOrder(order) {
    this.setState({
      order,
    }, this.handleChange);
  }

  handleChangeGenus(genus) {
    this.setState({
      genus,
    }, this.handleChange);
  }

  handleChangeSpecies(species) {
    this.setState({
      species,
    }, this.handleChange);
  }

  handleChange() {
    const { order, genus, species } = this.state;
    this.props.onChange({ order, genus, species });
  }

  render() {
    const { taxonomy, loading } = this.state;

    if (loading) {
      return <Loading />
    }
    return (
      <div>
        <p>Taxonomy</p>
        <OrderFilter onSelect={this.handleChangeOrder} data={taxonomy.order}/>
        <GenusFilter onSelect={this.handleChangeGenus} data={taxonomy.genus}/>
        <SpeciesFilter onSelect={this.handleChangeSpecies} data={taxonomy.species}/>
      </div>
    );
  }
}

export default TaxonomyFilter;

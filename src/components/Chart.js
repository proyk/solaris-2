import React, { Component } from 'react';
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  Brush
} from 'recharts';
import { getChartDataByFilter } from '../lib/getChartDataByFilter.js';
import axios from 'axios';
import Loading from './Loading';

class Chart extends Component {
  constructor() {
    super();

    this.state = {
      data: [],
      loading: true,
    };

    this.styles = {
      container: {
        width: '100%',
        height: '70vh',
        padding: 18,
      },
    };
  }
  componentDidMount() {
    this.jsonRequest =
      axios
      .get('https://s3-eu-west-1.amazonaws.com/proyk-uploads/Solaris-2.json')
      .then((result) => {
        this.setState({
          data: result.data || [],
          loading: false,
        });
      });
  }
  componentWillUnmount() {
    this.jsonRequest.abort();
  }
  render() {
    const { data, loading } = this.state;
    const { filters } = this.props;

    const chartData = getChartDataByFilter({ filters, data });

    if (loading) {
      return <Loading />
    }

    return (
      <div style={this.styles.container}>
        <h4>Number of creatures</h4>
        <ResponsiveContainer>
          <LineChart data={chartData} margin={{ top: 0, right: 30, left: 0, bottom: 0 }}>
            <XAxis dataKey="date" height={50} tickSize={15} />
            <YAxis />
            <CartesianGrid strokeDasharray="3 3"/>
            <Tooltip/>
            <Legend verticalAlign="top" height={30} />

            <Line type='monotone' dataKey='dead' stroke='#7f9a48' />
            <Line type='monotone' dataKey='alive' stroke='#40699c' />
            <Line type='monotone' dataKey='unknown' stroke='#9e413e' />

            <Line type='monotone' dataKey='young' stroke='#695185' />
            <Line type='monotone' dataKey='adult' stroke='#3c8da3' />
            <Line type='monotone' dataKey='elder' stroke='#cc7b38' />

            <Line type='monotone' dataKey='herbivore' stroke='#c0504d' />
            <Line type='monotone' dataKey='carnivore' stroke='#9bbb59' />

            <Brush />
          </LineChart>
        </ResponsiveContainer>
      </div>
    );
  }
}

export default Chart;


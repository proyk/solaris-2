import React from 'react';

const DietFilter = ({ onSelect }) => (
  <div>
    <label>Diet </label>
    <select onChange={(e) => onSelect(e.target.value)}>
      <option key={0} value="">All</option>
      <option key={1} value="herbivore">herbivore</option>
      <option key={2} value="carnivore">carnivore</option>
    </select>
  </div>
);

export default DietFilter;

import React from 'react';

const renderOptions = (data) => data.map((el, index) => (
  <option key={index} value={el}>{el}</option>
));

const OrderFilter = ({ onSelect, data=[] }) => (
  <div>
    <label>Order </label>
    <select onChange={(e) => onSelect(e.target.value)}>
      <option value="">All</option>
      {renderOptions(data)}
    </select>
  </div>
);

export default OrderFilter;

import React from 'react';

const StatusFilter = ({ onSelect }) => (
  <div>
    <label>Status </label>
    <select onChange={(e) => onSelect(e.target.value)}>
      <option key={0} value="">All</option>
      <option key={1} value="alive">Alive</option>
      <option key={2} value="dead">Dead</option>
      <option key={3} value="unknown">Unknown</option>
    </select>
  </div>
);

export default StatusFilter;


import React from 'react';
import StatusFilter from './StatusFilter';
import AgeFilter from './AgeFilter';
import DietFilter from './DietFilter';
import TaxonomyFilter from './TaxonomyFilter';

const styles = {
  container: {
    backgroundColor: '#c4c4c4',
    fontSize: '0.87em',
    width: 250,
    height: '80vh',
    padding: 18,
  },
};

const Filters = ({ onChangeStatusFilter, onChangeAgeFilter, onChangeDietFilter, onChangeTaxonomyFilter }) => (
  <div style={styles.container}>
    <h4>Filters</h4>
    <StatusFilter onSelect={onChangeStatusFilter} />
    <AgeFilter onSelect={onChangeAgeFilter} />
    <DietFilter onSelect={onChangeDietFilter} />
    <TaxonomyFilter onChange={onChangeTaxonomyFilter} />
  </div>
);

export default Filters;

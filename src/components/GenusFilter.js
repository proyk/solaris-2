import React from 'react';

const renderOptions = (data) => data.map((el, index) => (
  <option key={index} value={el}>{el}</option>
));

const GenusFilter = ({ onSelect, data=[] }) => (
  <div>
    <label>Genus </label>
    <select onChange={(e) => onSelect(e.target.value)}>
      <option value="">All</option>
      {renderOptions(data)}
    </select>
  </div>
);

export default GenusFilter;

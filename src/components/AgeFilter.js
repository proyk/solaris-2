import React from 'react';

const ageArr = ["young", "adult", "elder"];

const renderOptions = () => ageArr.map((el, index) => (
  <option key={index} value={el}>{el}</option>
));

const AgeFilter = ({ onSelect }) => (
  <div>
    <label>Age </label>
    <select onChange={(e) => onSelect(e.target.value)}>
      <option value="">All</option>
      {renderOptions()}
    </select>
  </div>
);

export default AgeFilter;

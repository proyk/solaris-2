import React, { Component } from 'react';
import './App.css';
import Chart from './Chart';
import Filters from './Filters';

class App extends Component {
  constructor() {
    super();

    this.state = {
      status: '',
      age: '',
      diet: '',
      taxonomy: '',
    };

    this.handleChangeStatusFilter = this.handleChangeStatusFilter.bind(this);
    this.handleChangeAgeFilter = this.handleChangeAgeFilter.bind(this);
    this.handleChangeDietFilter = this.handleChangeDietFilter.bind(this);
    this.handleChangeTaxonomyFilter = this.handleChangeTaxonomyFilter.bind(this);
  }

  handleChangeStatusFilter(status) {
    this.setState({ status });
  }

  handleChangeAgeFilter(age) {
    this.setState({ age });
  }

  handleChangeDietFilter(diet) {
    this.setState({ diet });
  }

  handleChangeTaxonomyFilter(taxonomy) {
    this.setState({ taxonomy });
  }

  render() {
    return (
      <div>
        <div className="App-header">
          <h2>Solaris 2</h2>
        </div>
        <div className="App-body">
          <Filters
            onChangeStatusFilter={this.handleChangeStatusFilter}
            onChangeAgeFilter={this.handleChangeAgeFilter}
            onChangeDietFilter={this.handleChangeDietFilter}
            onChangeTaxonomyFilter={this.handleChangeTaxonomyFilter}
          />
          <Chart filters={this.state} />
        </div>
      </div>
    );
  }
}

export default App;

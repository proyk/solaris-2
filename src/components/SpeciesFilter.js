import React from 'react';

const renderOptions = (data) => data.map((el, index) => (
  <option key={index} value={el}>{el}</option>
));

const SpeciesFilter = ({ onSelect, data=[] }) => (
  <div>
    <label>Species </label>
    <select onChange={(e) => onSelect(e.target.value)}>
      <option value="">All</option>
      {renderOptions(data)}
    </select>
  </div>
);

export default SpeciesFilter;

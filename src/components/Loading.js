import React from 'react';
import logo from './logo.svg';

const styles = {
  container: {
    textAlign: 'center',
    marginTop: 25,
    width: '100%',
  },
  text: {
    marginTop: 0,
  },
};

const Loading = () => (
  <div style={styles.container}>
    <img src={logo} className="App-logo" alt="logo" />
    <h4 style={styles.text}>Loading...</h4>
  </div>
);

export default Loading;

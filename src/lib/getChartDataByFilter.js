import moment from 'moment';

const filterDataByTaxonomy = ({ data, filters }) => {
  const taxonomy = [];

  if (filters.order) {
    taxonomy.push(filters.order);
  }

  if (filters.genus) {
    taxonomy.push(filters.genus);
  }

  if (filters.species) {
    taxonomy.push(filters.species);
  }

  return taxonomy.every(elem => data.indexOf(elem) > -1);
};

const getFilteredData = (data, filters) => {
  return data.filter((obj) =>  {
    return Object.keys(filters).every((c) => {
      if (!filters[c]) {
        return true;
      }

      if (Array.isArray(obj[c])) {
        return filterDataByTaxonomy({ data: obj[c], filters: filters[c] });
      }

      return obj[c] === filters[c];
    });
  });
};

const getStatus = ({ data = [], filters }) => {
  return getFilteredData(data, filters).reduce((memo, obj) => {
    for (var i in memo) {
      if (~obj.status.indexOf(i)) {
        memo[i]++;
      }

      if (~obj.age.indexOf(i)) {
        memo[i]++;
      }

      if (~obj.diet.indexOf(i)) {
        memo[i]++;
      }
    }
    return memo;
  },{ alive: 0, dead: 0, unknown: 0, young: 0, adult:0, elder:0, herbivore:0, carnivore: 0 });
};

const getFormattedDate = (isoStringDate) => {
  return moment(isoStringDate).format('YYYY-MM-DD');
};

export const getChartDataByFilter = ({ data, filters }) => {
  return data.map((el) => {
    const date = getFormattedDate(el[0]);
    const status = getStatus({ data: el[1], filters });

    return { date, ...status };
  })
};
